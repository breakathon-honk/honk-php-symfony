<?php

namespace App\Controller;

use App\Entity\HonkPizza;
use App\Form\HonkPizzaType;
use App\Repository\HonkPizzaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/honk/pizza")
 */
class HonkPizzaController extends AbstractController
{
    /**
     * @Route("/", name="honk_pizza_index", methods={"GET"})
     */
    public function index(HonkPizzaRepository $honkPizzaRepository): Response
    {
        return $this->render('honk_pizza/index.html.twig', [
            'honk_pizzas' => $honkPizzaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="honk_pizza_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $honkPizza = new HonkPizza();
        $form = $this->createForm(HonkPizzaType::class, $honkPizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($honkPizza);
            $entityManager->flush();

            return $this->redirectToRoute('honk_pizza_index');
        }

        return $this->render('honk_pizza/new.html.twig', [
            'honk_pizza' => $honkPizza,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="honk_pizza_show", methods={"GET"})
     */
    public function show(HonkPizza $honkPizza): Response
    {
        return $this->render('honk_pizza/show.html.twig', [
            'honk_pizza' => $honkPizza,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="honk_pizza_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HonkPizza $honkPizza): Response
    {
        $form = $this->createForm(HonkPizzaType::class, $honkPizza);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('honk_pizza_index');
        }

        return $this->render('honk_pizza/edit.html.twig', [
            'honk_pizza' => $honkPizza,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="honk_pizza_delete", methods={"POST"})
     */
    public function delete(Request $request, HonkPizza $honkPizza): Response
    {
        if ($this->isCsrfTokenValid('delete'.$honkPizza->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($honkPizza);
            $entityManager->flush();
        }

        return $this->redirectToRoute('honk_pizza_index');
    }
}
