<?php

namespace App\DataFixtures;

use App\Entity\HonkPizza;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $honkPizza = new HonkPizza();
        $honkPizza->setName('4-cheeses');
        $manager->persist($honkPizza);
        $manager->flush();
    }
}
